package com.example.buttons;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private AnimationDrawable anim1,anim2,anim3,anim4,anim5,anim6,anim7,anim8;
    private Button daneOsobowe;
    private Button podzialGodzin;
    private Button rozkladAutobusu;
    private Button oceny;
    private Button kwestura;
    private Button informacjeDodatkowe;
    private Button wyszukiwarka;
    private Button ogloszenia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        anim1 = (AnimationDrawable) podzialGodzin.getBackground();
        anim1.setEnterFadeDuration(2300);
        anim1.setExitFadeDuration(2300);

        anim2 = (AnimationDrawable) ogloszenia.getBackground();
        anim2.setEnterFadeDuration(2300);
        anim2.setExitFadeDuration(2300);

        anim3 = (AnimationDrawable) daneOsobowe.getBackground();
        anim3.setEnterFadeDuration(2300);
        anim3.setExitFadeDuration(2300);

        anim4 = (AnimationDrawable) kwestura.getBackground();
        anim4.setEnterFadeDuration(2300);
        anim4.setExitFadeDuration(2300);

        anim5 = (AnimationDrawable) oceny.getBackground();
        anim5.setEnterFadeDuration(2300);
        anim5.setExitFadeDuration(2300);

        anim6 = (AnimationDrawable) informacjeDodatkowe.getBackground();
        anim6.setEnterFadeDuration(2300);
        anim6.setExitFadeDuration(2300);

        anim7 = (AnimationDrawable) wyszukiwarka.getBackground();
        anim7.setEnterFadeDuration(2300);
        anim7.setExitFadeDuration(2300);

        anim8 = (AnimationDrawable) rozkladAutobusu.getBackground();
        anim8.setEnterFadeDuration(2300);
        anim8.setExitFadeDuration(2300);


    }

    private void init() {
        this.daneOsobowe = findViewById(R.id.daneOsobowe);
        daneOsobowe.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this,DaneOsobowe.class));
            }
        });
        this.podzialGodzin = findViewById(R.id.podzialGodzin);
        podzialGodzin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this,PodzialGodzin.class));
            }
        });
        this.rozkladAutobusu = findViewById(R.id.rozkladAutobusu);
        rozkladAutobusu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this,RozkladAutobusu.class));
            }
        });
        this.oceny = findViewById(R.id.oceny);
        oceny.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this,Oceny.class));
            }
        });
        this.kwestura = findViewById(R.id.kwestura);
        kwestura.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this,Kwestura.class));
            }
        });
        this.informacjeDodatkowe = findViewById(R.id.informacjeDodatkowe);
        informacjeDodatkowe.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this, InformacjeDodatkowe2.class));
            }
        });
        this.wyszukiwarka = findViewById(R.id.wyszukiwarka);
        wyszukiwarka.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this,Wyszukiwarka.class));
            }
        });
        this.ogloszenia = findViewById(R.id.ogloszenia);
        ogloszenia.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainActivity.this,Ogloszenia.class));
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(anim1!=null && !anim1.isRunning()){
            anim1.start();
        }
        if(anim2!=null && !anim2.isRunning()){
            anim2.start();
        }
        if(anim3!=null && !anim3.isRunning()){
            anim3.start();
        }
        if(anim4!=null && !anim4.isRunning()){
            anim4.start();
        }
        if(anim5!=null && !anim5.isRunning()){
            anim5.start();
        }
        if(anim6!=null && !anim6.isRunning()){
            anim6.start();
        }
        if(anim7!=null && !anim7.isRunning()){
            anim7.start();
        }
        if(anim8!=null && !anim8.isRunning()){
            anim8.start();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(anim1!=null && anim1.isRunning()){
            anim1.stop();
        }
        if(anim2!=null && anim2.isRunning()){
            anim2.stop();
        }
        if(anim3!=null && anim3.isRunning()){
            anim3.stop();
        }
        if(anim4!=null && anim4.isRunning()){
            anim4.stop();
        }
        if(anim5!=null && anim5.isRunning()){
            anim5.stop();
        }
        if(anim6!=null && anim6.isRunning()){
            anim6.stop();
        }
        if(anim7!=null && anim7.isRunning()){
            anim7.stop();
        }
        if(anim8!=null && anim8.isRunning()){
            anim8.stop();
        }

    }
}
