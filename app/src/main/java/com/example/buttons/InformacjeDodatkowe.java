package com.example.buttons;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class InformacjeDodatkowe extends AppCompatActivity {

    InformacjeDodatkowe2 backgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.informacje_dodatkowe);
        backgr = (InformacjeDodatkowe2) findViewById(R.id.backgr);
        backgr.setMargins(800, 500);
        backgr.setMultipliers(2.5f, 2.7f);
    }
    protected void onStart(){
        super.onStart();

    }
    protected void onPause() {
        super.onPause();
        backgr.onPause();
    }
    protected void onResume() {
        super.onResume();
        backgr.onResume();
    }
}
